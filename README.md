# Evaluacion-Oralsys

# Evaluation instructions

Front (Sólo para personas destinadas a este grupo)

Crear un proyecto Angular 5 donde se haga uso de Google Oauth2, con inicio de sesión y un pequeño Dashboard con los datos del perfil de Google y los archivos en Drive, en donde se debe poder listarlos y acceder a ellos; en el Dashboard se debe tener un registro de actividad de la cuenta de Google Drive asociada. A medida que se vaya avanzando se debe ir registrando los cambios con Git y luego subir el código a un repositorio en gitlab y enviarnos la URL. Se tomará en cuenta cualquier añadido o función extra que pueda ser desarrollada bajo el tiempo establecido, el diseño debe ser responsabilidad total del desarrollador.

A evaluar:
Creatividad y diseño.
Resolución de problemas.
Código limpio y bien documentado.


# For testing app

run "npm install"

run "ng serve"

go to http://localhost:4200
