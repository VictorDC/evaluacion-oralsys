import { PipeTransform, Pipe } from '@angular/core';

@Pipe({
    name: 'translateMonth'
})

/**
    Translate month name to spanish.

    @param value The date.
    @return The date with month translated to spanish.
*/
export class TranslateMonthPipe implements PipeTransform {
    transform( value: any ) {
         return value.replace(/January|February|March|April|May|June|July|August|September|October|November|December/gi, function (x) {
            switch (x) {
                case 'January':
                    return 'Enero';
                case 'February':
                    return 'Febrero';
                case 'March':
                    return 'Marzo';
                case 'April':
                    return 'Abril';
                case 'May':
                    return 'Mayo';
                case 'June':
                    return 'Junio';
                case 'July':
                    return 'Julio';
                case 'August':
                    return 'Agosto';
                case 'September':
                    return 'Septiembre';
                case 'October':
                    return 'Octubre';
                case 'November':
                    return 'Noviembre';
                case 'December':
                    return 'Diciembre';
            }
        });
    }
}
