import { LoginOpt, AuthServiceConfig, GoogleLoginProvider } from 'angularx-social-login';

/**
 * Return The settings to access the google API.
 * @return config The settings.
 */
export function ProvideConfig() {

    const googleLoginOptions: LoginOpt = {
        scope: 'profile email https://www.googleapis.com/auth/drive'
    };

    const config = new AuthServiceConfig([
        {
        id: GoogleLoginProvider.PROVIDER_ID,
        provider: new GoogleLoginProvider('355725658184-e06k8r0drhdfet98vb54f8jiqqhdifra.apps.googleusercontent.com', googleLoginOptions),
        }
    ]);

    return config;
}
