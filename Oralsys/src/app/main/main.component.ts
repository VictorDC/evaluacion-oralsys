import { Component, OnInit, Inject } from '@angular/core';
import { AuthService, SocialUser } from 'angularx-social-login';
import { GoogleLoginProvider } from 'angularx-social-login';
import { HttpClient } from '@angular/common/http';
import { DOCUMENT } from '@angular/platform-browser';
import {
  trigger,
  state,
  style,
  transition,
  animate,
  keyframes
} from '@angular/animations';


@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css'],
  animations: [
    trigger('list', [
      state('in', style({
        opacity: 1,
        transform: 'translateX(0)'
      })),
      transition( 'void => *', [
        animate(800, keyframes([
          style({
            opacity: 0,
            transform: 'translateX(-500px)',
            offset: 0
          }),
          style({
            opacity: 0.5,
            transform: 'translateX(100px)',
            offset: 0.8
          }),
          style({
            opacity: 1,
            transform: 'translateX(0)',
            offset: 1
          })
        ]))
      ])
    ])
  ]
})


export class MainComponent implements OnInit {
  /** The authenticated user */
  user: SocialUser;
  /** The authentication user token */
  token: string;
  /** Array of files from Google Drive */
  files: any;
  /** Array of changes information of Google Drive files */
  changes: any;
  /** A specific document from Google Drive */
  doc: any;
  /** Auxiliary variables to show / hide html elements */
  list = false;
  activity = false;
  /** Auxiliary variables to paginate html list */
  p = 1;
  p2 = 1;
  /** Input phrase to be searched in filter.pipe.ts */
  filteredListName = '';

  constructor(
    @Inject(DOCUMENT) private document: any,
    private authService: AuthService,
    private http: HttpClient
  ) {}

  ngOnInit() {
    this.authService.authState.subscribe((user) => {
      this.user = user;
      if (user) {
        this.token = user.authToken;
        this.getFiles(this.token);
        this.getActivity();
      }
    });
  }

  /** Authenticate with Google Oauth2 */
  signInWithGoogle() {
    this.authService.signIn(GoogleLoginProvider.PROVIDER_ID);
  }

  /** Sing off with Google Oauth2 */
  signOut() {
    this.authService.signOut();
    this.list = false;
    this.activity = false;
  }

  /** Get array of files from Google Drive */
  getFiles(token) {
    this.http.get('https://www.googleapis.com/drive/v3/files?access_token=' + token ).subscribe(
      (files) => {
        this.files = files;
        this.files = this.files.files;
      }
    );
  }

  /** Get array of changes from Google Drive */
  getActivity() {
    this.http.get('https://www.googleapis.com/drive/v2/changes?access_token=' + this.token ).subscribe(
      (activity) => {
        this.changes = activity;
        this.changes = this.changes.items;
      }
    );
  }

  /**
   * Get a specific document from Google Drive and redirect to specified drive document
   * @param id specific document id
   */
  getDoc(id) {
    this.http.get('https://www.googleapis.com/drive/v2/files/' + id + '/?access_token=' + this.token).subscribe(
      (doc) => {
        this.doc = doc;
        this.document.location.href = this.doc.alternateLink;
      }
    );
  }

  /**
   * Redirect to specified drive document
   * @param url specific document url to redirect
   */
  goDoc(url) {
    this.document.location.href = url;
  }

  /** Show / hide list of files */
  showList() {
    this.list = true;
    this.activity = false;
  }

  /** Show / hide list of changes */
  showActivity() {
    this.list = false;
    this.activity = true;
  }

}
