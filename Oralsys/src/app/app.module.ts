import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { NgxPaginationModule } from 'ngx-pagination';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { SocialLoginModule, AuthServiceConfig,  } from 'angularx-social-login';

import { AppComponent } from './app.component';
import { ProvideConfig } from './googleLoginConfig';
import { MainComponent } from './main/main.component';
import { FooterComponent } from './footer/footer.component';
import { TranslateMonthPipe } from './translateMonth.pipe';
import { FilterPipe } from './filter.pipe';



@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
    FooterComponent,
    TranslateMonthPipe,
    FilterPipe
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    SocialLoginModule,
    NgxPaginationModule,
    BrowserAnimationsModule
  ],
  providers: [
    {
      provide: AuthServiceConfig,
      useFactory: ProvideConfig
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
