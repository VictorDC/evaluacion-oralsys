import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filter',
  pure: false
})

/**
    Returns array of objects where the specified filtersString match with the specified propName value of the origin array.

    @param value The array of objects where be searched.
    @param filterString The phrase to be searched.
    @param propName The property of the objects in the origin array.
    @return The new array of objects only with the coincidences.
*/
export class FilterPipe implements PipeTransform {

  transform(value: any, filterString: string, propName: string ): any {
    if (value.lenght === 0 || filterString === '' || filterString === undefined) {
      return value;
    }

    const a = filterString.toUpperCase();
    const resultArray = [];

    for ( const item of value) {
      const b = item[propName].toUpperCase();
      const index = b.indexOf(a);
      if ( index >= 0 ) {
        resultArray.push(item);
      }
    }

    return resultArray;
  }

}
